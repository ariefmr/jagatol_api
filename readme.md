SETUP AWAL
=========

Aplikasi JAGATOL API.

### SETUP APLIKASI CODEIGNITER:
  - Clone via source tree ke folder jagatol_api di htdocs
  - buat folder assets/third_party/
  - Extract semua zip file di folder misc/bahan_awal/
  - file assets_awal.zip mengandung tiga folder (css, js, images), Copy/paste tiga folder ini ke dalam folder assets/third_party/
  - extract file zip system.zip dan paste di root folder dev_kapi
  - extract file zip config_awal.zip (config.php, database.php), Copy/Paste ke folder application/config/
  - buat folder assets/devnila/cs, assets/devnila/images/, assets/devnila/js

### SETUP DATABASE MYSQL:
  - Masuk ke phpmyadmin
  - Buat database jagatol

optional
  - Buat folder uploads/document di root folder jagatol_api
### SETUP MPDF:
  - Download MPDF : [MPDF57.zip](http://mpdf1.com/repos/MPDF57.zip)
  - Extract MPDF57.zip, rename jadi folder mpdf. MOVE ke folder application/libraries/