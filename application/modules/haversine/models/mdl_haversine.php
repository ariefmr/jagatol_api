<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_haversine extends CI_Model
{
	private $db_dss;
	private $db_kapi;

    function __construct()
    {
        $this->load->database();
    }

	public function get_all_lat_lang()
    {
    	$query = "select ruas_tol_id, id_toll_route, lat, lang, gerbang_tol_name
                    from toll_route";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function get_restarea_by_ruas()
    {
        $query = "SELECT * FROM toll_restarea";

        $run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }

    public function from_kapi()
    {
    	$query = "";

    	$run_query = $this->db->query($query);                            
        
        if($run_query->num_rows() > 0){
            $result = $run_query->result();
        }else{
            $result = false;
        }
        return $result;
    }
}