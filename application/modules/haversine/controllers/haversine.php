<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Haversine extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
			parent::__construct();
			$this->load->config('menus');
			$this->load->config('db_timeline');
			$this->load->config('globals');
			$this->load->model('mdl_haversine');
	}
	
	public function getAllLatLang()
	{
		$arr_lat_lang = $this->mdl_haversine->get_all_lat_lang();

		$arr_hasil = array();
		$i = 0;
		foreach ($arr_lat_lang as $data) {
			foreach ($data as $key => $value) {
				$arr_hasil[$i][$key] = $value;
			}
			$i++;
		}

		// echo "<pre>";
		// var_dump($arr_hasil);
		// echo "</pre>";

		return $arr_hasil;

	}
	
	public function getAllRestArea()
	{
		$arr_lat_lang = $this->mdl_haversine->get_restarea_by_ruas();

		$arr_hasil = array();
		$i = 0;
		foreach ($arr_lat_lang as $data) {
			foreach ($data as $key => $value) {
				$arr_hasil[$i][$key] = $value;
			}
			$i++;
		}

		// echo "<pre>";
		// var_dump($arr_hasil);
		// echo "</pre>";

		return $arr_hasil;

	}

	function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
		$earth_radius = 6371;
		
		$dLat = deg2rad($latitude2 - $latitude1);
		$dLon = deg2rad($longitude2 - $longitude1);
		
		$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
		$c = 2 * asin(sqrt($a));
		$d = $earth_radius * $c;
		
		return ceil($d);

	}

	function getHaversine($par_lat, $par_lang){
		$arr_lat_lang = $this->getAllLatLang();
		$jarak_temp = 9999;
		$arr_unsort = array();
		$i = 0;
		foreach ($arr_lat_lang as $data) {
			$lat = $data['lat'];
			$lang = $data['lang'];
			$id = $data['id_toll_route'];
			$nama = $data['gerbang_tol_name'];
			$jarak = $this->getDistance($par_lat, $par_lang, $lat, $lang);
			
			$arr_unsort[$i] = array(
							'id' => $id,
							'jarak' => $jarak,
							'nama' => $nama
							);

			$i++;
		}

		return $arr_unsort;
		
	}

	function getJarakRestArea($par_lat, $par_lang){
		$arr_restarea = $this->getAllRestArea();
		$jarak_temp = 9999;
		$arr_unsort = array();
		$i = 0;
		foreach ($arr_restarea as $data) {
			$lat = $data['lat'];
			$lang = $data['lang'];
			$id = $data['id_toll_restarea'];
			$nama = $data['km'];
			
			$jarak = $this->getDistance($par_lat, $par_lang, $lat, $lang);
			
			$arr_unsort[$i] = array(
							'id' => $id,
							'jarak' => $jarak,
							'nama' => $nama
							);

			$i++;
		}

		return $arr_unsort;
		
	}

	function swap(&$arr, $a, $b) {
	    $tmp = $arr[$a];
	    $arr[$a] = $arr[$b];
	    $arr[$b] = $tmp;
	}

	function bubble_sort($arr_unsort) {
	    $size = count($arr_unsort);
	    for ($i=0; $i<$size; $i++) {
	        for ($j=0; $j<$size-1-$i; $j++) {
	            if ($arr_unsort[$j+1]['jarak'] < $arr_unsort[$j]['jarak']) {
	                $this->swap($arr_unsort, $j, $j+1);
	            }
	        }
	    }
	    return $arr_unsort;
	} 

	function sorting_jarak($par_lat, $par_lang){
		$arrGate = $this->getHaversine($par_lat, $par_lang);
		// echo $arr[3]['jarak'];
		$sortingGate = $this->bubble_sort($arrGate);

		$id_terdekat = $sortingGate[0]['id'];
		$jarak_terdekat = $sortingGate[0]['jarak'];
		$nama_terdekat = $sortingGate[0]['nama'];
		$id_terjauh = $sortingGate[count($sortingGate) - 1]['id'];
		$jarak_terjauh = $sortingGate[count($sortingGate) - 1]['jarak'];
		$nama_terjauh = $sortingGate[count($sortingGate) - 1]['nama'];

		$arrRestarea = $this->getJarakRestArea($par_lat, $par_lang);
		$sortingRestarea = $this->bubble_sort($arrRestarea);

		$id_restarea_terdekat = $sortingRestarea[0]['id'];
		$jarak_restarea_terdekat = $sortingRestarea[0]['jarak'];
		$nama_restarea_terdekat = $sortingRestarea[0]['nama'];
		$id_restarea_terjauh = $sortingRestarea[count($sortingRestarea) - 1]['id'];
		$jarak_restarea_terjauh = $sortingRestarea[count($sortingRestarea) - 1]['jarak'];
		$nama_restarea_terjauh = $sortingRestarea[count($sortingRestarea) - 1]['nama'];

		$info_jarak = array(	'id_terdekat' => $id_terdekat, 
								'jarak_terdekat' => $jarak_terdekat,
								'nama_terdekat' => $nama_terdekat,
								// 'id_terjauh' => $id_terjauh,
								// 'jarak_terjauh' => $jarak_terjauh,
								// 'nama_terjauh' => $nama_terjauh,
								'id_restarea_terdekat' => $id_restarea_terdekat, 
								'jarak_restarea_terdekat' => $jarak_restarea_terdekat,
								'nama_restarea_terdekat' => $nama_restarea_terdekat,
								// 'id_restarea_terjauh' => $id_restarea_terjauh,
								// 'jarak_restarea_terjauh' => $jarak_restarea_terjauh,
								// 'nama_restarea_terjauh' => $nama_restarea_terjauh
							);

		echo "<pre>";
		var_dump($info_jarak);
		echo "</pre>";
	}

}
